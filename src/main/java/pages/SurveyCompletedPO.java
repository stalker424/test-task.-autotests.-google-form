package pages;

import org.openqa.selenium.By;

public class SurveyCompletedPO extends BasePO {

    private static final String REL_URL = "/forms/d/e/1FAIpQLSd9rsonv4thf50pjjZzc8zGmyJ8EPIvQBAtNHpl4tuW0_TyOA/formResponse";
    public SurveyCompletedPO() { super(REL_URL);}

    private final By ASK_AGAIN_SEL = By.cssSelector(".freebirdFormviewerViewResponseLinksContainer a");

    //клікути на "Отправить ещё один ответ"
    public void clickAskAgain(){
        clickElement(ASK_AGAIN_SEL);
    }
}
